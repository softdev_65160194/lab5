/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Player {
    private char symbol;
    private int winCount=0,loseCount=0 , drawCount=0;
    

    public Player(char symbol) {
       this.symbol = symbol;
    }


    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }
    
    public char getSymbol() {
        return symbol;
    }   
    
    public void win() {
        winCount++;
    }
    
    public void lose() {
        loseCount++;
    }
    
    public  void draw() {
        drawCount++;
    }

    
}
