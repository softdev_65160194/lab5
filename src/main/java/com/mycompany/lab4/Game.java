/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;
import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class Game {
    private Table table;
    private Player player1,player2;
    
        
    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }
    
    public void play() {
        showWelcome();
        newGame();
        while(true){
            showTable();
            showturn();
            inputrowcol();
            if(table.checkWin()){
                showTable();
                showstatus();
                newGame();
            }if(table.checkdraw()){
                showTable();
                showstatus();
                newGame();
            }
            
            
        }
    }   

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    
    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                System.out.print(t[i][j]+" ");
            }
            System.out.println();
        }
    }
    private void newGame() {
        table = new Table(player1,player2);
    }
    private void inputrowcol() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.println("Please input row");
            int row = kb.nextInt();
            row = row-1;
            System.out.println("Please input col");
            int col = kb.nextInt();
            col = col-1;
            if(table.setRowCol(row , col)){
                break;
            }else{
                System.out.println("Invalid input. Please try again.");
            }
        }
    }
    private void showstatus() {
        System.out.println("Symbol : "+player1.getSymbol()+" Win : "+player1.getWinCount()+" Lose : "+player1.getLoseCount()+" Draw : "+player1.getDrawCount());
        System.out.println("Symbol : "+player2.getSymbol()+" Win : "+player2.getWinCount()+" Lose : "+player2.getLoseCount()+" Draw : "+player2.getDrawCount());
    }

    private void showturn() {
        System.out.println(table.getcurrentplayer().getSymbol()+" Turn");
    }
}
