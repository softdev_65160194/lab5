/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Table {
    private char [][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player player1;
    private Player player2;
    private Player currentplayer;
    private int turnCount = 0;
    private int col;
    private int row;
    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentplayer = player1;
    }
    
    public char [][] getTable() {
        return table;
    }
    public Player getcurrentplayer() {
        return currentplayer;
    }

    public boolean setRowCol(int row , int col) {
        if(checkWin())return false;
        if (table[row][col] != '-') return false;
        table[row][col] = currentplayer.getSymbol();
        this.row = row;
        this.col = col;
        
        if(checkWin()){
            countwinlose();
            return false;
        }if(checkdraw()){
            countdraw();
            return false;
        }
        swithplayer();
        return true;
    }
    
    public boolean checkWin() {
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }
        if(checkdiago1()){
            return true;
        }
        if(checkdiago2()){
            return true;
        }
        return false;
    }
    
    public boolean checkRow(){
        return table[row][0] == table[row][1] && table[row][1] == table[row][2] && table[row][2] != '-';
    }
    
    public boolean checkCol(){
        return table[0][col] == table[1][col] && table[1][col] == table[2][col] && table[2][col] != '-';
    }
    
    public boolean checkdiago1(){
        return table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-';
    }
    
    public boolean checkdiago2(){
        return table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[0][2] != '-';
    }
    public void swithplayer(){
        turnCount++;
        if(currentplayer == player1){
            currentplayer = player2;
        }
        else{
            currentplayer = player1;
        }
    }
    public void countwinlose(){
        if(currentplayer != player1){
            player1.lose();
            player2.win();
        }else{
            player2.lose();
            player1.win();
        }
    }
    public void countdraw(){
        if(checkdraw()){
            player1.draw();
            player2.draw();
        }
    }
    public boolean checkdraw(){
        if(turnCount == 8){
            return true;
        }
        return false;
    }
}
